package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.request.DataSaveBinaryRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;

public final class DataSaveBinaryCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in binary file";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getDomainEndpoint().saveDataBinary(new DataSaveBinaryRequest(getToken()));
    }
}
