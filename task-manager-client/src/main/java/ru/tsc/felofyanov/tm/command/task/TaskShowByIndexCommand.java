package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.dto.request.TaskGetByIndexRequest;
import ru.tsc.felofyanov.tm.dto.response.TaskGetByIndexResponse;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken(), index);
        @NotNull final TaskGetByIndexResponse response =
                getServiceLocator().getTaskEndpoint().getTaskByIndex(request);
        @Nullable final TaskDTO task = response.getTask();
        showTask(task);
    }
}
