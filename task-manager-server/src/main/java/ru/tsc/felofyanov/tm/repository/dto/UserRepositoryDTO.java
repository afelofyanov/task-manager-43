package ru.tsc.felofyanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager, UserDTO.class);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable String login) {
        final TypedQuery<UserDTO> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE login = :login", clazz)
                        .setParameter("login", login);
        @NotNull final List<UserDTO> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable String email) {
        final TypedQuery<UserDTO> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE email = :email", clazz)
                        .setParameter("email", email);
        @NotNull final List<UserDTO> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public UserDTO removeByLogin(@Nullable String login) {
        @Nullable final Optional<UserDTO> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }
}
