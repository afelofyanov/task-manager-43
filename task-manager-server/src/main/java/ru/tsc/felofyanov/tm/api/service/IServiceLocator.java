package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.felofyanov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.felofyanov.tm.api.service.dto.IUserServiceDTO;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IUserServiceDTO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();
}
