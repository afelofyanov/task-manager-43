package ru.tsc.felofyanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.enumerated.Role;

public interface IUserServiceDTO extends IServiceDTO<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO createWithEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO createWithRole(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    @Nullable
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    UserDTO updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);
}
