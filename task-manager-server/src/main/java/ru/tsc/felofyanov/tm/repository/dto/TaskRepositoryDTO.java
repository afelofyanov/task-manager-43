package ru.tsc.felofyanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepositoryDTO extends AbstractUserOwnerRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    public TaskRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager, TaskDTO.class);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("FROM " + getModelName() + " WHERE user_id = :userId AND project_Id = :projectId", clazz)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        entityManager.createQuery("FROM " + getModelName() + " WHERE user_id = :userId AND project_Id = :projectId", clazz)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .executeUpdate();
    }
}
