package ru.tsc.felofyanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskServiceDTO extends IUserOwnerServiceDTO<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(@Nullable String userId, @NotNull String projectId);
}
