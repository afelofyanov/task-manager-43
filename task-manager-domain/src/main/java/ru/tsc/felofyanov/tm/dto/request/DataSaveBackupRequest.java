package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveBackupRequest extends AbstractUserRequest {

    public DataSaveBackupRequest(@Nullable String token) {
        super(token);
    }
}
